from django.shortcuts import render, redirect, get_list_or_404
from .forms import *


#CRUD

#create

def contact_new(request, template_name='newcontact.html'):
    form = ContactForm(request.POST or None, request.Files or None)

    dados = {'form': form}

    if form.is_valid():
        form.save()
        return redirect('contact_list')
    return render(request, template_name, dados)

#READ
def contact_list(request, template_name='home.html'):
    contact = Contact.objects.all()
    dados = {'contact': contact}
    return render(request, template_name, dados)

#UPDATE
def contact_update(request, id, template_name='contact_form.html'):
    contact = get_list_or_404(Contact, pk=id)
    form = ContactForm(request.POST or None, request.Files or None, instance=contact)

    dados = {'form':form}

    if form.is_valid():
        form.save()
        return render(request, template_name, dados)


#delete
def contact_delete(request, id, template_name='contact_delete.html'):
    contact = get_list_or_404(Contact, pk=id)

    if request.method == 'POST':
        contact.delete()

        return redirect('contact_list')
    return render(request, template_name)



